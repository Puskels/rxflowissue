//
//  RxFlow.swift
//  RxFlowTest
//
//  Created by McSims on 25/01/2020.
//  Copyright © 2020 MP. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxFlow

enum AppStep: Step {
    case start
    case nextScreen
}

class AppFlow: Flow, Stepper {

    var steps = PublishRelay<Step>()

    var root: Presentable {
        return self.rootViewController
    }

    private let rootViewController = UIViewController()
    private let rootWindow: UIWindow?

    init(withWindow window: UIWindow?) {
        rootWindow = window
    }

    func navigate(to step: Step) -> FlowContributors {
        guard let step = step as? AppStep else { return .none }

        switch step {

        case .start:
            return navigateToStart()
        case .nextScreen:
            return navigateToNextScreen()
        }
    }

    private func navigateToStart() -> FlowContributors {
        rootViewController.view.backgroundColor = .red
        rootWindow?.rootViewController = rootViewController
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            self.rootViewController.steps.accept(AppStep.nextScreen)
        })
        return .one(flowContributor: .contribute(withNextPresentable: rootViewController, withNextStepper: rootViewController))
    }

    private func navigateToNextScreen() -> FlowContributors {
        let flow = NextFlow(controller: rootViewController)

        Flows.whenReady(flow1: flow) { [unowned self] (root) in
            self.rootViewController.present(root, animated: true)

            let options: UIView.AnimationOptions = .transitionFlipFromLeft
            UIView.transition(with: self.rootViewController.view, duration: 0.5, options: options, animations: nil, completion: nil)
        }
        return .one(flowContributor: .contribute(withNextPresentable: flow, withNextStepper: flow.rootViewController))
    }
}

class NextFlow: Flow {

    var root: Presentable {
        return self.rootViewController
    }

    let rootViewController: UIViewController

    private let presentingController: UIViewController

    init(controller: UIViewController) {
        let vc = UIViewController()
        vc.view.backgroundColor = .blue
        rootViewController = vc

        presentingController = controller
    }

    func navigate(to step: Step) -> FlowContributors {
        return .none
    }
}

let _steps = PublishRelay<Step>()

extension UIViewController: Stepper {
    public var steps: PublishRelay<Step> {
        return _steps
    }
}
